import * as React from 'react';
import { Text, View } from 'react-native';
import { styles } from './Styles';

import {
  LineChart
} from "react-native-chart-kit";

import { Dimensions } from "react-native";


import { connect } from 'react-redux'

const initialState = { 
  data: [{ channel: 'CH1', mean: 0 },
         { channel: 'CH2', mean: 0 },
         { channel: 'CH3', mean: 0 },
         { channel: 'CH4', mean: 0 },
         { channel: 'CH5', mean: 0 },
         { channel: 'CH6', mean: 0 },
         { channel: 'CH7', mean: 0 },
         { channel: 'CH8', mean: 0 }            
        ]
}

/*
class DataShow extends React.Component<{}> {

    constructor(props) {
      super(props);
      this.state = {
        data: initialState
      };
    }  

    NSAFE_componentWillMount = () => {
      this.setState({
        data: this.props.data
      })
    }  
    render() { 
      const  { data }   = this.props
      //console.log("this.props=", this.props )
      //console.log("data=", data ) 

      

      return (
        <View>
        {
          data.map((d, index) => (
            <View key={index}>
              <Text>{d.channel}:{d.mean}</Text>
            </View>
          ))
        }
        </View>     
      )
    }
  }      
*/


class DataShow extends React.Component<{}>  {
  

  constructor(props) {
    super(props);

    this.cnt = 0
    this.chartData = //new Array() 
    [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
    ]
    

    this.state = {
      data: initialState,
    };
/*
    setInterval(()=> {
      this.setState({
        chartData: [
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10,
            Math.random() * 10  
        ] 
      });
    }, 5000);
*/    
  } 

  
  NSAFE_componentWillMount = () => {
  //  console.log("WillMount: ", this.props.data)
  //  this.setState({
  //    data: this.props.data
  //  })
  } 
  
  getMean = () => {
    let sum = 0
    let i
    for (i=0; i < this.chartData.length; ++i) sum += this.chartData[i]
    //console.log(sum/this.chartData.length)
    return sum/this.chartData.length 
  }

  render() {

    const  { data }   = this.props
    const point = data.find(item => item.channel == this.props.name)
    let cnt = this.cnt

    if (++cnt>=50) cnt=0
    this.cnt = cnt 
    
    
    let prev
    const alpha = 1.0
    prev = cnt ?  this.chartData[cnt-1] : this.chartData[this.chartData.length-1]
    let p = point.mean
    if (isNaN(prev)) prev = p
    //this.chartData[cnt] = (Math.abs(this.getMean()-p)>5000) ?  prev : p
    this.chartData[cnt] = alpha*p + (1-alpha)*prev



    //this.chartData[cnt] = point.mean

    const chartdata = {
  //    labels: ["1м.", "2м.", "3м.", "4м.", "5м.", 
  //             "6м.", "7м.", "8м.", "9м.", "10м."
  //            ],
      datasets: [
        {
          data: this.chartData
        }
     ]
    }
   

    return (
      <View>
        <Text style={styles.measureTitle}>{this.props.name}</Text>          
        <LineChart
          withDots = {false}
          withShadow = {false}
          withInnerLines = {false}
          withOuterLines = {false}        
          data = {chartdata}
          width={Dimensions.get("window").width-10} // from react-native
          height={2*Dimensions.get("window").height/3}
          yAxisLabel=""
          yAxisSuffix=""
          yLabelsOffset= {-Dimensions.get("window").width+100}
          chartConfig={{
            backgroundColor:this.props.color,//"#e26a00",
            backgroundGradientFrom:this.props.color,// "#fb8c00",
            backgroundGradientTo:this.props.color,// "#ffa726",
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => 'khaki',//`rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) =>'red',//'khaki',//`rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 10
            },
            propsForDots: {
              r: "3",
              strokeWidth: "2",
              stroke: 'black'//"#ffa726"
            }
          }}
          //bezier
          //  style={{
          //    marginVertical: 0,
          //    borderRadius: 10
          //  }}
        />
      </View>
    );
  }
}
               
  
const mapStateToProps = (state) => ({
    data: state.sensorDataReducer.data
})
  
export default connect(mapStateToProps) (DataShow)