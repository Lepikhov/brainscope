import AsyncStorage  from '@react-native-async-storage/async-storage'


export default class DataStorage {


  static storeData = async (value, key) => {
    
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
      // saving error
      console.log('error from AsyncStorage: ', e)
    }
    
  }


  static getData = async (key) => {
    try {
      const jsonValue = await AsyncStorage.getItem(key)
      //console.log('getData:', jsonValue)
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value 
      console.log('error from AsyncStorage: ', e)     
    }
  }

  static clear = async () => {
    AsyncStorage.clear();
  }

  static getAllKeys = async () => {
    const keys = await AsyncStorage.getAllKeys()
    .then(keys => console.log(keys))
  }

}

