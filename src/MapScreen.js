import * as React from 'react';
import { Button, ScrollView, Text, Image, View } from 'react-native';
import { Dimensions } from "react-native";
import { styles } from './Styles';
import BrainMap from '../src/BrainMap';



export function MapScreen({ route, navigation }) {
  return (
    <ScrollView style={styles.ScrollView}>  
      <Text style={styles.mapName}>Измерения</Text>
      <BrainMap name='measures'/>          
      <Text style={styles.mapName}>Эталон</Text>        
      <BrainMap name='reference'/>      
      <Text style={styles.mapName}>Тепловая карта</Text> 
      <Text style={styles.mapName}>(измерения и эталон)</Text>      
      <Image style={{ 
        width: Dimensions.get("window").width, 
        height: 50 
      }}      
        source={require('../res/picker.png')}>
      </Image>
      <View style={styles.mapViewLabel}>
        <Text style={styles.mapLabel}>0мВ</Text> 
        <Text style={{...styles.mapLabel, textAlign: 'right'}}>10мВ</Text> 
      </View>   
      <Text style={styles.mapName}>Отклонения от нормы</Text>        
      <BrainMap name='difference'/>       
      <Text style={styles.mapName}>Тепловая карта (отклонения)</Text>       
      <Image style={{ 
        width: Dimensions.get("window").width, 
        height: 50 
      }}      
        source={require('../res/picker.png')}>
      </Image>
      <View style={styles.mapViewLabel}>
        <Text style={styles.mapLabel}>-5мВ</Text> 
        <Text style={{...styles.mapLabel, textAlign: 'right'}}>+5мВ</Text> 
      </View>       
    </ScrollView>
  );
}

