import * as React from 'react';
import { StyleSheet, View, TouchableOpacity, Alert, Text } from 'react-native';
import { RNCamera } from 'react-native-camera'


import { connect } from 'react-redux'



export class CameraScreen extends React.Component {  

  constructor(props) {
    super(props);

    this.state = {
      uri: ''
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.exit.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14, color: 'white' }}> ВЫЙТИ </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14, color: 'white' }}>       ФОТО      </Text>
          </TouchableOpacity>
          <TouchableOpacity onPressOut={this.saveAndExit.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14, color: 'white' }}>СОХРАНИТЬ И ВЫЙТИ</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
      this.setState({uri:data.uri})
    }
  };

  exit () {
    this.props.navigation.navigate('Patient')
  } 


  saveAndExit () {
    //console.log(this.state.uri)
    this.props.navigation.navigate('Patient',
    {uri: this.state.uri}
    )
  } 

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    height: 50,
    backgroundColor: 'dodgerblue',
    borderRadius: 25,
    borderColor: 'black',    
    borderWidth: 0,
    padding: 15,
    paddingHorizontal: 5,
    alignSelf: 'center',
    margin: 5,
  },
});





