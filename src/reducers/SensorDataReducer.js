import { UPDATE_DATA } from '../actions'
import { act } from 'react-test-renderer'

const initialState = { 
  data: [{ channel: 'CH1', mean: 0 },
         { channel: 'CH2', mean: 0 },
         { channel: 'CH3', mean: 0 },
         { channel: 'CH4', mean: 0 },
         { channel: 'CH5', mean: 0 },
         { channel: 'CH6', mean: 0 },
         { channel: 'CH7', mean: 0 },
         { channel: 'CH8', mean: 0 }            
        ]
} 



const sensorDataReducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_DATA:
      //console.log("action.data =", action.data )
      //state.data = action.data
      return {
        data: [
          ...action.data
        ]
      }
    default:
      return state;
  }   
  
}

export default sensorDataReducer