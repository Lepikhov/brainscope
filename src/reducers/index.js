import { combineReducers } from 'redux'

import sensorDataReducer from './SensorDataReducer'

const rootReducer = combineReducers({
    sensorDataReducer
})

export default rootReducer