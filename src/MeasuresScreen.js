import * as React from 'react';
import { Button, ScrollView, View, Text, Image } from 'react-native';
import { styles } from './Styles';


import SensorsComponent  from '../src/SensorsComponents';
import DataShow   from '../src/DataShow';



/*
export function MeasuresScreen({ route, navigation }) {
  return (
    <ScrollView style={styles.ScrollView}>
      <SensorsComponent/>
      <DrawChart name='Fz' color='green'/>
      <DrawChart name='Cz' color='red'/> 
      <DrawChart name='Oz' color='orange'/>
      <DrawChart name='Td' color='green'/>
      <DrawChart name='Ts' color='blue'/>
    </ScrollView>
  );
}
*/

export function MeasuresScreen({ route, navigation }) {
  return (
    <ScrollView style={styles.ScrollView}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <SensorsComponent/>
        <DataShow name='CH1' color='black'/>       
        <DataShow name='CH2' color='black'/> 
        <DataShow name='CH3' color='black'/>       
        <DataShow name='CH4' color='black'/> 
        <DataShow name='CH5' color='black'/>       
        <DataShow name='CH6' color='black'/> 
        <DataShow name='CH7' color='black'/>       
        <DataShow name='CH8' color='black'/>  
      </View> 
    </ScrollView>
  );
}



