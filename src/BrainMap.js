import Svg, {
  Circle,
  Ellipse,
  G,
  Text,
  TSpan,
  TextPath,
  Path,
  Polygon,
  Polyline,
  Line,
  Rect,
  Use,
  Image,
  Symbol,
  Defs,
  LinearGradient,
  RadialGradient,
  Stop,
  ClipPath,
  Pattern,
  Mask,
} from 'react-native-svg';

import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';



export default class SvgExample extends React.Component {
  

  data = [
    { name: 'measures', means: [1.3, 2.2, 4.1, 6.7, 8.9] },           
    { name: 'reference', means: [5.1, 5.1, 4.9, 5.2, 5.6] },   
    { name: 'difference', means: [-3.8, -2.7, -0.8, 1.5, 3.3] },         
  ]

  constructor(props) {
    super(props);
  }  

  colormap = require('colormap')

  colors = this.colormap({
    colormap: 'jet',
    nshades: 100,
    format: 'hex',
    alpha: 1
  })

  toColor(num, off) {
    num += off
    if (num > 9.9) num = 9.9
    if (num < 0) num = 0
    return this.colors[Math.round(num*100.0/10.0)]
  }

  render() { 

    let t = [0.0, 0.0, 0.0, 0.0, 0.0]

    let off = 0.0
    if (this.props.name == 'difference') off = 5.0

    try {
      t = this.data.find(item => item.name == this.props.name).means
    } 
    catch (err)
    {
    }

    return (
      <View>
        <Svg height={Dimensions.get("window").width} width={Dimensions.get("window").width}>
          <Image
            x="0"
            y="0"
            width={Dimensions.get("window").width}
            height={Dimensions.get("window").width}
            preserveAspectRatio="xMidYMid slice"
            opacity="1.0"
            href={require('../res/X-brain.png')}
            clipPath="url(#clip)"
          />             
          <Circle
            cx={5+Dimensions.get("window").width/2}
            cy={20+Dimensions.get("window").width/6}
            r={Dimensions.get("window").width/10}
            opacity="0.7"
            strokeWidth="0.0"
            fill={this.toColor(t[0], off)}
          />
          <Text x={-25+Dimensions.get("window").width/2} y={25+Dimensions.get("window").width/6} 
            fill="white" 
            fontSize={Dimensions.get("window").width/20}
          >
            {t[0] + " мВ"}
          </Text>    
          <Circle
            cx={5+Dimensions.get("window").width/2}
            cy={-85+Dimensions.get("window").width}
            r={Dimensions.get("window").width/10}
            opacity="0.7"
            strokeWidth="0.0"
            fill={this.toColor(t[1], off)}
          />
          <Text x={-25+Dimensions.get("window").width/2} y={-78+Dimensions.get("window").width} 
            fill="white" 
            fontSize={Dimensions.get("window").width/20}
          >
            {t[1] + " мВ"}
          </Text>          
          <Circle
            cx={10+Dimensions.get("window").width/4}
            cy={5+Dimensions.get("window").width/2}
            r={Dimensions.get("window").width/10}
            opacity="0.7"
            strokeWidth="0.0"
            fill={this.toColor(t[2], off)}
          />   
          <Text x={-20+Dimensions.get("window").width/4} y={10+Dimensions.get("window").width/2} 
            fill="white" 
            fontSize={Dimensions.get("window").width/20}
          >
            {t[2] + " мВ"}
          </Text>             
          <Circle
            cx={-0+3*Dimensions.get("window").width/4}
            cy={5+Dimensions.get("window").width/2}
            r={Dimensions.get("window").width/10}
            opacity="0.7"
            strokeWidth="0.0"
            fill={this.toColor(t[3], off)}
          />   
          <Text x={-30+3*Dimensions.get("window").width/4} y={10+Dimensions.get("window").width/2}
            fill="white" 
            fontSize={Dimensions.get("window").width/20}
          >
              {t[3] + " мВ"}     
          </Text>  
          <Circle
            cx={5+Dimensions.get("window").width/2}
            cy={5+Dimensions.get("window").width/2}
            r={Dimensions.get("window").width/10}
            opacity="0.7"
            strokeWidth="0.0"
            fill={this.toColor(t[4], off)}
          />
          <Text x={-25+Dimensions.get("window").width/2} y={10+Dimensions.get("window").width/2} 
            fill="white" 
            fontSize={Dimensions.get("window").width/20}
          >
              {t[4] + " мВ"}
          </Text>            
        </Svg>
      </View>
    );
  }
}




