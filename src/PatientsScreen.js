import * as React from 'react';
import {
  Button,
  SafeAreaView,
  View,
  ScrollView,
  Text,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { styles } from './Styles';
import { PatientScreen } from './PatientScreen';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';


import { PatientsData, getPatients, storePatients, setPatients } from './Patients';

export var CurPatient = '872f7285-2a25-43d1-a48d-19ecd3f40ce8';

export const DEAFAULT_PATIENT_URI = 'https://journal.ib-bank.ru/files/images/news/mozg.jpg'



export function CheckDeletedPatient(id) {
  if (id === CurPatient) {
    var i = 0;
    while (i < PatientsData.length) {
      if (PatientsData[i].id === id) {
        break;
      }
      ++i;
    }
    if (i >= PatientsData.length)
      if (PatientsData.length) CurPatient = PatientsData[0].id
  }
}


const Item = ({ item }) => (
  <View>
    <Text style={styles.patientTitle}>
      {item.title}
    </Text>
    <View style={styles.patientImageContainer}>
      <Image style={styles.patientImage} source={{ uri: item.image }}>
      </Image>
    </View>
    <Text style={styles.patient}>
      Дата рождения: {item.birthDay}
    </Text>
    <Text style={styles.patientDiagnosis}>
      Диагноз: {item.diagnosis}
    </Text>
  </View>
);


export function PatientsScreen({ route, navigation }) {

  const [isFirstShow, setIsFirstShow] = React.useState(true);

  React.useEffect(() => {

    (async () => {      
      if (isFirstShow) {
        setIsFirstShow(false)
      }  
      else {
        await storePatients()
      }  
    })()

    if (route.params?.item) {
      CheckDeletedPatient(route.params.item.id)
    }
  }, [route.params?.item]);

  const renderItem = ({ item }) => (
    <TouchableOpacity style={{
      alignItems: 'center', justifyContent: 'center',
      borderWidth: 0, borderColor: 'black', borderRadius: 0,
      paddingTop: 10, paddingBottom: 10
    }}
      onPress={() => {
        CurPatient = item.id
        navigation.navigate('Patient', { item: item })
      }}
    >
      <Item item={item} />
    </TouchableOpacity>
  );


  return (
    <View alignItems='center' flex={1} >
      <TouchableOpacity style={styles.mainMenuButton}
        onPress={() => {
          let p = {
            id: uuidv4(),
            title: '',
            birthDay: moment(new Date()).format('DD-MM-YYYY'),
            diagnosis: '',
            image: DEAFAULT_PATIENT_URI,
          }
          PatientsData.push(p)
          navigation.navigate('Patient', { item: p })
        }}
      >
        <Text style={styles.mainMenuButtonText}>ДОБАВИТЬ ПАЦИЕНТА</Text>
      </TouchableOpacity>
      <SafeAreaView style={styles.ScrollView}>
        <FlatList contentContainerStyle={{ paddingBottom: 100 }}
          data={PatientsData}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  );
}


