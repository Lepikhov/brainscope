import * as React from 'react';
import { 
  StyleSheet, 
  Platform,
  Dimensions 
} from 'react-native';

export const styles = StyleSheet.create({
  scrollView: {
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center'  
  },   
  mainMenu: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center'
  },  
  mainMenuButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get("window").width - 20,
    height: 60,
    backgroundColor: 'dodgerblue',
    borderColor: 'black',
    borderWidth: 0,
    borderRadius: 30,
    marginTop: 2,
    marginBottom: 2
  },
  mainMenuButtonText: {
    justifyContent: 'center',
    color: 'white'
  }, 
  secondaryButton: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center',
    width: 3*Dimensions.get("window").width/4,
    height: 50,
    backgroundColor: 'dodgerblue',
    borderColor: 'black',
    borderWidth: 0,
    borderRadius: 25,
    marginTop: 5
  },
  secondaryButtonText: {
    justifyContent: 'center',
    color: 'white'
  },  
  patientImageContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").width,
    borderRadius: 1,
    marginTop: 0,
    paddingTop: 20,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: {
            height: 10,
        },
        shadowOpacity: 1
      },
      android: {
        borderWidth: 0,
        borderColor: 'black',
        elevation: 15
      }
    })
  },
  patientImage: {
    width: Dimensions.get("window").width - 40,
    height: Dimensions.get("window").width - 40,
    marginTop: 0,
    paddingTop: 0,
    resizeMode: 'cover'
  },
  patientTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 0,
    marginBottom: 10,
    marginLeft: 10    
  },   
  measureTitle: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 0,
  },   
  mapName: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 25,
    marginTop: 0,
  },  
  mapViewLabel: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  mapLabel: {
    flex: 1,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 20
  },
  patient: {
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 0,
    marginLeft: 10  
  },
  patientDiagnosis: {
    fontWeight: 'bold',    
    fontStyle: 'italic',
    color: 'red',
    marginTop: 10,
    marginRight: 0,
    marginLeft: 10,
    marginBottom: 0
  },
  aboutMethodTitle: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 0,
  },  
  aboutMethod: {
    fontWeight: 'bold',    
    fontStyle: 'italic',
    color: 'black',
    fontSize: 16,    
    marginTop: 0,
    marginRight: 0,
    marginLeft: 0,
    marginBottom: 0
  },
  datePickerStyle: {
    width: 100,
    marginTop: 20,
    marginBottom: 10,
    paddingLeft: 10,
    borderRadius: 0,
    borderWidth: 0,
    borderColor: 'black',
    fontWeight: 'bold',    
    fontStyle: 'italic',
    color: 'black',
    fontSize: 16
  }
});