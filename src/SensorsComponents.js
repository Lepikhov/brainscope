import React, { Component } from 'react';
import { Platform, View, Text, ScrollView, PermissionsAndroid } from 'react-native';
import { BleManager } from 'react-native-ble-plx';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { updateData } from './actions';

const initialState = { 
  data: [{ channel: 'CH1', mean: 0 },
         { channel: 'CH2', mean: 0 },
         { channel: 'CH3', mean: 0 },
         { channel: 'CH4', mean: 0 },
         { channel: 'CH5', mean: 0 },
         { channel: 'CH6', mean: 0 },
         { channel: 'CH7', mean: 0 },
         { channel: 'CH8', mean: 0 },            
        ]
} 

class SensorsComponent extends React.Component {

  //sdata = initialState

  constructor() {
    super()
    //this.manager = new BleManager()
    this.state = {info: "", values: {}, data:initialState}
    this.prefixUUID = "cdfeec4"
    this.suffixUUID = "-9511-11ea-ab12-0800200c9a66"
    this.connected=false
    this.sensors = {
      0: "Brain Data1"//, 
    //  1: "Brain Data2" //,
    }

    this.timerId = setInterval(()=> {
      if (this.connected) 
        this.read(this.device)
          .catch(() => {
          })
    }, 100);
  }

  serviceUUID(num) {
    //return this.prefixUUID + num + "0" + this.suffixUUID
    return "181A"
  }

  notifyUUID(num) {
    //return this.prefixUUID + num + this.suffixUUID
    //return "7d54be46-df31-4a0a-b0c3-2534ba73221a"
    return "cdfeec40-9511-11ea-ab12-0800200c9a66"
  }

  writeUUID(num) {
    //return this.prefixUUID + num + "2" + this.suffixUUID
  }

  info(message) {
    this.setState({info: message})
  }

  error(message) {
    this.setState({info: "ERROR: " + message})
  }

  threeByteOneComplementTranslate( b1, b2, b3 ) {
    let tmp = (b1<<16)+(b2<<8)+b3
    return (tmp & 0x800000) ? -((0x7fffff & (~tmp)) + 1) : tmp 
  }

  updateValue(key, value) {
    this.setState({values: {...this.state.values, [key]: value}})
    //console.log("value=", value)    
  }

  UNSAFE_componentWillMount() {    

    console.log("WillMount")

    this.manager = new BleManager()

    if (Platform.OS === 'ios') {
      this.manager.onStateChange((state) => {
        if (state === 'PoweredOn') this.scanAndConnect()
      })
    } else {
      this.scanAndConnect()
    }
  }

  componentWillUnmount() {    
    console.log("WillUnMount")

    clearInterval(this.timerId)
    this.connected=false
    if (this.device) this.device.cancelConnection()
                       .then(() => { this.manager.destroy()})
  }  

  scanAndConnect() {
    const permission = this.requestLocationPermission();
    if (permission) {      
      this.manager.startDeviceScan(null,
                                   null, (error, device) => {
        this.info("Scanning...")
        //console.log(device)
        this.device = device                            

        if (error) {
          this.error(error.message)
          return
        }

        //if (device.name === 'TI BLE Sensor Tag' || device.name === 'SensorTag') {
        //if (device.name === 'MI Band 2') {    
        if (device.name === 'brain_controller') {      
          this.info("Connecting to Sensor")
          this.manager.stopDeviceScan()
          
          device.connect()
            .then((device) => {
              this.info("Discovering services and characteristics")
              return device.discoverAllServicesAndCharacteristics()
            })
            /*.then((device) => {
              this.info("Setting notifications")
              //return this.setupNotifications(device)
            })
            .then(() => {
              this.info("Listening...")
            }, (error) => {
              this.error(error.message)
            })*/
            .then(() => {
              this.connected = true
            })  
        }
      });
    }
  }

  async setupNotifications(device) {
    for (const id in this.sensors) {
      const service = "181A"//this.serviceUUID(id)
//      const characteristicW = this.writeUUID(id)
      const characteristicN = this.notifyUUID(id)
      console.log(characteristicN)

      //const characteristic = await device.writeCharacteristicWithResponseForService(
      //  service, characteristicW, "AQ==" /* 0x01 in hex */
      //)
      
      device.monitorCharacteristicForService(service, characteristicN, (error, characteristic) => {
        if (error) {
          this.error(error.message)
          return
        }
        this.updateValue(characteristic.uuid, characteristic.value)
        const val = characteristic.value;
        const buf = Buffer.from(val,'base64')
        console.log(`uuid=${characteristic.uuid}`,"buf=", buf ) 
/*
        var sdata = [
         { channel: 'Fz', mean: this.threeByteOneComplementTranslate(buf[3],buf[4],buf[5]) },
         { channel: 'Cz', mean: this.threeByteOneComplementTranslate(buf[6],buf[7],buf[8]) },
         { channel: 'Oz', mean: this.threeByteOneComplementTranslate(buf[9],buf[10],buf[11]) },
         { channel: 'Td', mean: this.threeByteOneComplementTranslate(buf[12],buf[13],buf[14]) },
         { channel: 'Ts', mean: this.threeByteOneComplementTranslate(buf[15],buf[16],buf[17]) }   
        ]

        //console.log("sdata=", sdata ) 
        //this.props.dispatchUpdateData(this.state.data)
        this.setState({data: sdata})
        this.props.updateData(this.state.data)
        //console.log("data=", this.state.data )
*/        
               
      })
    }
  }

  async read(device) {

    if (!device.isConnected()) 
    {
      return
    }   
    const service = "181A"//this.serviceUUID(id)
//  const characteristicW = this.writeUUID(id)
    const characteristicN = "cdfeec40-9511-11ea-ab12-0800200c9a66"
//    console.log(characteristicN)

      //const characteristic = await device.writeCharacteristicWithResponseForService(
      //  service, characteristicW, "AQ==" /* 0x01 in hex */
      //)
      
    characteristic = await device.readCharacteristicForService(service, characteristicN)
    {

    /*  if (error) {
        this.error(error.message)
        this.connected = false
        return
      }
    */  
      this.updateValue(characteristicN, characteristic.value)
      const val = characteristic.value;
      const buf = Buffer.from(val,'base64')
      //console.log(`uuid=${characteristic.uuid}`,"buf=", buf ) 

        var sdata = [
         { channel: 'CH1', mean: this.threeByteOneComplementTranslate(buf[3],buf[4],buf[5]) },
         { channel: 'CH2', mean: this.threeByteOneComplementTranslate(buf[6],buf[7],buf[8]) },
         { channel: 'CH3', mean: this.threeByteOneComplementTranslate(buf[9],buf[10],buf[11]) },
         { channel: 'CH4', mean: this.threeByteOneComplementTranslate(buf[12],buf[13],buf[14]) },
         { channel: 'CH5', mean: this.threeByteOneComplementTranslate(buf[15],buf[16],buf[17]) },
         { channel: 'CH6', mean: this.threeByteOneComplementTranslate(buf[18],buf[19],buf[20]) },
         { channel: 'CH7', mean: this.threeByteOneComplementTranslate(buf[21],buf[22],buf[23]) },
         { channel: 'CH8', mean: this.threeByteOneComplementTranslate(buf[24],buf[25],buf[26]) }            
        ]

        //console.log("sdata=", sdata ) 
        //this.props.dispatchUpdateData(this.state.data)
        this.setState({data: sdata})
        this.props.updateData(this.state.data)
        //console.log("data=", this.state.data )
      }    
               
  }
  

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION, {
        //PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {    
          title: 'Location permission for bluetooth scanning',
          message: 'wahtever',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      ); 
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Location permission for bluetooth scanning granted');
        return true;
      } else {
        console.log('Location permission for bluetooth scanning revoked');
        return false;
      }
    } catch (err) {
      console.warn(err);
      return false;
    }
  }

  render() {
    //const { data } = this.props
    //console.log("______________this.props=", this.props )

    return (
      <View>       
        {Object.keys(this.sensors).map((key) => {
          return <Text key={key}>
                   {this.sensors[key] + ": " + (this.state.values[this.notifyUUID(key)] || "-")}
                 </Text>
        })}       
      </View>
      
    )
  }
}


const mapStateToProps = (state) => ({
  data: state.sensorDataReducer.data
})

//const mapDispatchToProps = {
//  dispatchUpdateData: (data) => updateData(data)
//}


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    updateData,
  }, dispatch)
);


export default connect(mapStateToProps, mapDispatchToProps) (SensorsComponent)

