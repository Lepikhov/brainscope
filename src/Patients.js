import { func } from 'prop-types';
import DataStorage  from './DataStorage'


const PATIENTS_DATA_KEY = "@BrainScopePatientsDataKey"

/*
export const PatientsData = [
  {
    id: '1',
    title: 'Путин Владимир Владимирович',
    birthDay: '07.10.1952',
    diagnosis: 'отец нации',
    image: require('../res/pu.png'),
  },
  {
    id: '2',
    title: 'Шойгу Сергей Кожугетович',
    birthDay: '21.05.1955',
    diagnosis: 'герой',
    image: require('../res/sho.png'),
  },
  {
    id: '3',
    title: 'Петросян Евгений Ваганович',
    birthDay: '16.09.1945',
    diagnosis: 'шутник',
    image: require('../res/va.png'),
  },
  {
    id: '4',
    title: 'Киркоров Филипп Бедросович',
    birthDay: '30.04.1967',
    diagnosis: 'румын',
    image: require('../res/ki.jpg'),
  },
  {
    id: '5',
    title: 'Бузова Ольга Игоревна',
    birthDay: '20.01.1986',
    diagnosis: 'звезда',
    image: require('../res/bu.jpg'),
  },
  {
    id: '6',
    title: 'Панасенков Евгений Николаевич',
    birthDay: '13.03.1982',
    diagnosis: 'гений',
    image: require('../res/pa.jpg'),
  },
  {
    id: '7',
    title: 'Че',
    birthDay: '18.03.2014',
    diagnosis: 'патриот',
    image: require('../res/che.png'),
  },
];
*/


export let PatientsData = []
/*
[
  {
    id: '1067a7cde-e845-40a8-b811-45f30475febd',
    title: 'Путин Владимир Владимирович',
    birthDay: '07.10.1952',
    diagnosis: 'отец нации',
    image: 'https://icdn.lenta.ru/images/2021/03/18/19/20210318195926829/square_320_bb4b277b65949db59997d7938661b118.jpg',
  },
  {
    id: '07a20538-4977-43ff-8ca8-6089361a592f',
    title: 'Шойгу Сергей Кожугетович',
    birthDay: '21.05.1955',
    diagnosis: 'герой',
    image: 'https://img.gazeta.ru/files3/720/13955720/RIAN_6630902.HR-pic_32ratio_900x600-900x600-49466.jpg',
  },
  {
    id: '7807920e-8191-4ad5-9db2-6ca3106e2cb3',
    title: 'Петросян Евгений Ваганович',
    birthDay: '16.09.1945',
    diagnosis: 'шутник',
    image: 'https://s0.rbk.ru/v6_top_pics/media/img/0/52/755333853548520.jpg',
  },
  {
    id: '872f7285-2a25-43d1-a48d-19ecd3f40ce8',
    title: 'Киркоров Филипп Бедросович',
    birthDay: '30.04.1967',
    diagnosis: 'румын',
    image: 'https://cdnimg.rg.ru/img/content/192/80/40/4p_kirkorov_centr_d_850.jpg',
  },
  {
    id: '067a7cde-e845-40a8-b811-45f30475febd',
    title: 'Бузова Ольга Игоревна',
    birthDay: '20.01.1986',
    diagnosis: 'звезда',
    image: 'https://regnum.ru/uploads/pictures/news/2019/07/26/regnum_picture_1564124461100413_normal.jpg',
  },
  {
    id: '1c0c20f1-0011-4c98-a507-675f94bf73ce',
    title: 'Панасенков Евгений Николаевич',
    birthDay: '13.03.1982',
    diagnosis: 'гений',
    image: 'https://static.news.ru/photo/a735830a-1782-11eb-bf18-fa163e074e61_760.jpg',
  },
  {
    id: '690d866b-bd7a-47c5-bf04-a4ca17a03e68',
    title: 'Че',
    birthDay: '18.03.2014',
    diagnosis: 'патриот',
    image: 'http://risovach.ru/upload/2015/07/generator/cheburashka-vatnik_87583667_orig_.jpg',
  },
];

*/

export async function getPatients() {
  let p = await DataStorage.getData(PATIENTS_DATA_KEY)
  return p
}


export async function storePatients() {
  await DataStorage.storeData(PatientsData, PATIENTS_DATA_KEY)
  //await DataStorage.clear()
}

export function setPatients(p) {
  PatientsData = [...PatientsData, ...p]
}

