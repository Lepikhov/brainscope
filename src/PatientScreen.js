import * as React from 'react'
import { ScrollView, View, Text, TextInput, Image, TouchableOpacity } from 'react-native'
import { PatientsData } from './Patients'
import { styles } from './Styles'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'


export function PatientScreen({ route, navigation }) {

  const [title, setTitle] = React.useState(route.params.item.title)
  const [birthDay, setBirthDay] = React.useState(route.params.item.birthDay)
  const [date, setDate] = React.useState(new Date(strToDDMMYYYY(route.params.item.birthDay)))
  const [diagnosis, setDiagnosis] = React.useState(route.params.item.diagnosis)
  const [image, setImage] = React.useState(route.params.item.image)
  const [showDP, setShowDP] = React.useState(false)


  function strToDDMMYYYY (str) {
      return str.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2/$1/$3")
  }

  const onChangeDatePicker = (event, selectedDate) => {
    const currentDate = selectedDate || date
    setShowDP(Platform.OS === 'ios')
    setDate(currentDate)
    setBirthDay(moment(currentDate).format('DD.MM.YYYY'))

  };


  const showDatepicker = () => {
    setShowDP(true);
  };


  React.useEffect(() => {
    if (route.params?.uri) {
      setImage(route.params.uri)
    }
  }, [route.params?.uri]);


  return (
    <ScrollView style={styles.ScrollView}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={styles.patientTitle}>
          ПАЦИЕНТ
        </Text>
        <View style={styles.patientImageContainer}>
          <Image style={styles.patientImage} source={{ uri: image }} />
        </View>
        <TouchableOpacity style={styles.secondaryButton}
          onPress={() => {
            navigation.navigate(
              'Camera',
              { item: route.params.item }
            );
          }}
        >
          <Text style={styles.secondaryButtonText}>СДЕЛАТЬ ФОТО</Text>
        </TouchableOpacity>
        <Text style={styles.patient}>
          ФИО:
        </Text>
        <TextInput
          style={styles.patient}
          placeholder="ФИО"
          onChangeText={setTitle}
          value={title}
        >
        </TextInput>
        <Text style={styles.patient}>
          Дата рождения:
        </Text>
        <View>
          <View>
            <Text
              style={styles.datePickerStyle}
              onPress={showDatepicker}
            >
              {moment(date).format('DD.MM.YYYY')}
            </Text>
          </View>
          <View>
            {showDP && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={'date'}
                is24Hour={true}
                display="spinner"
                onChange={onChangeDatePicker}
              />
            )}
          </View>
        </View>
        <Text style={styles.patientDiagnosis}>
          Диагноз:
        </Text>
        <TextInput
          multiline
          style={styles.patientDiagnosis}
          placeholder="Диагноз"
          onChangeText={setDiagnosis}
          value={diagnosis}
        >
        </TextInput>
        <TouchableOpacity style={styles.secondaryButton}
          onPress={() => {
            const item = route.params.item
            item.title = title
            item.birthDay = birthDay
            item.diagnosis = diagnosis
            item.image = image
            navigation.navigate(
              'Patients',
              { item: item }
            );
          }}
        >
          <Text style={styles.secondaryButtonText}>СОХРАНИТЬ И ВЫЙТИ</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.secondaryButton}
          onPress={() => {
            var i = 0;
            while (i < PatientsData.length) {
              if (PatientsData[i].id === route.params.item.id) {
                PatientsData.splice(i, 1)
                break
              }
              ++i;
            }
            navigation.navigate(
              'Patients',
              { item: route.params.item }
            );
          }}
        >
          <Text style={styles.secondaryButtonText}>УДАЛИТЬ</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}




