
import 'react-native-gesture-handler';
import * as React from 'react';
import { 
  View, 
  Text, 
  Image, 
  StyleSheet, 
  TouchableHighlight,
  Platform,
  Button,
  TouchableOpacity
} from 'react-native';
// import { Button, Header } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//import {BasicButton, FullButton} from '@phomea/react-native-buttons';
import { styles } from './src/Styles';

import { PatientsData, getPatients, setPatients } from './src/Patients'
import { CurPatient } from './src/PatientsScreen'


import { PatientsScreen } from './src/PatientsScreen';
import { PatientScreen } from './src/PatientScreen';
import { CameraScreen } from './src/CameraScreen';
import { MeasuresScreen } from './src/MeasuresScreen';
import { MapScreen } from './src/MapScreen';
import { AnalysisScreen } from './src/AnalysisScreen';
import { AboutMethodScreen } from './src/AboutMethodScreen';
import { SettingsScreen }  from './src/SettingsScreen';

import rootReducer from './src/reducers'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { title } from 'process';
const store = createStore(rootReducer)


function HomeScreen({ navigation }) {
  return (
    <View style={styles.mainMenu}>
      <TouchableOpacity style={styles.mainMenuButton}        
        onPress={() => {
          navigation.navigate('Patients', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      > 
        <Text style={styles.mainMenuButtonText}>ПАЦИЕНТЫ</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.mainMenuButton}          
        onPress={() => {
          navigation.navigate('Measures', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      >  
        <Text style={styles.mainMenuButtonText}>ИЗМЕРЕНИЯ</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.mainMenuButton}        
        onPress={() => {
          navigation.navigate('Map', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      >    
        <Text style={styles.mainMenuButtonText}>КАРТА</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.mainMenuButton}         
        onPress={() => {
          navigation.navigate('Analysis', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      > 
        <Text style={styles.mainMenuButtonText}>АНАЛИЗ</Text>
      </TouchableOpacity>   
      <TouchableOpacity style={styles.mainMenuButton}        
        onPress={() => {
          /* 1. Navigate to the Details route with params */
          navigation.navigate('AboutMethod', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      >   
        <Text style={styles.mainMenuButtonText}>О МЕТОДЕ</Text>
      </TouchableOpacity>   
      <TouchableOpacity style={styles.mainMenuButton}
         
        onPress={() => {
          /* 1. Navigate to the Details route with params */
          navigation.navigate('Settings', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
      >    
        <Text style={styles.mainMenuButtonText}>НАСТРОЙКИ</Text>
      </TouchableOpacity>       
    </View>
  );
}



const Stack = createStackNavigator();

function GetCurPatientTitle(cp) {
  var i = 0
  while (i < PatientsData.length) {
    if (PatientsData[i].id === cp) {
      return PatientsData[i].title   
    }
    ++i
  }
  return '' 
}

function GetCurPatientDiagnosis(cp) {
  var i = 0
  while (i < PatientsData.length) {
    if (PatientsData[i].id === cp) {
      return PatientsData[i].diagnosis   
    }
    ++i
  }
  return '' 
}

import { DEAFAULT_PATIENT_URI } from './src/PatientsScreen';

function GetCurPatientImage(cp) {
  var i = 0
  while (i < PatientsData.length) {
    if (PatientsData[i].id === cp) {
      return PatientsData[i].image   
    }
    ++i
  }
  return DEAFAULT_PATIENT_URI 
}



function LogoTitle() {
  return (
    <View style={{ flexDirection: 'row', flexWrap: 'wrap'}}>
      <Image
        style={{ width: 40, height: 40, marginRight: 20 }}
        source={{uri:GetCurPatientImage(CurPatient)}}
      /> 
      <View>
        <Text style={{marginTop: 1, fontSize: 12}}> 
          {'Пациент: '+GetCurPatientTitle(CurPatient)}
        </Text>        
      </View>
    </View>   
  );
}

function App() {

  const [isFirstShow, setIsFirstShow] = React.useState(true);

  React.useEffect(() => {
    (async () => {      
      if (isFirstShow) {
        const p = await getPatients()
        setIsFirstShow(false)
        if (p != null) {
          setPatients(p)
        }
      }  
      else {
      }  
    })()

  }, []);
  
  return (
    <Provider store={store} >
      <NavigationContainer>
        <Stack.Navigator 
          initialRouteName="Home"
          screenOptions={{
            headerStyle: {
              //backgroundColor: '#f4511e',
              backgroundColor: 'dodgerblue',
            },
              headerTintColor: '#fff',
          }}
        >
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />,
            }}
          />
          <Stack.Screen
            name="Patients"
            component={PatientsScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}
          />
          <Stack.Screen
            name="Patient"
            component={PatientScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}
          />
          <Stack.Screen
            name="Camera"
            component={CameraScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}
          />          
          <Stack.Screen
            name="Measures"
            component={MeasuresScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}
          />      
          <Stack.Screen
            name="Map"
            component={MapScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}
          />         
          <Stack.Screen
            name="Analysis"
            component={AnalysisScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}          
          /> 
          <Stack.Screen
            name="AboutMethod"
            component={AboutMethodScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}          
          />  
          <Stack.Screen
            name="Settings"
            component={SettingsScreen}
            options={{ 
              //title:"Главное меню"
              headerTitle: props => <LogoTitle {...props} />, 
            }}          
          />                       
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}



export default App;
